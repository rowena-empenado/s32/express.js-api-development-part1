const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");

// Check if email exists
module.exports.checkEmailExists = (reqBody) => {

    return User.find({email: reqBody.email}).then( result => {

        if(result.length > 0) {
            return true
        } else {
            return false
        }
    })
}

// User registration
module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
        /* 10 is the value provided as the number of "salt" rounds that the bcrypt algo will run in order to encrpyt the password
        hashSync(<dataToBeHashed><salt>) */
    })

    return newUser.save().then((user, error) => {

        if(error) {
            return false
        } else {
            return true
        }
    })
}

// User authentication
module.exports.loginUser = (reqBody) => {

    return User.findOne({email: reqBody.email}).then(result => {

        if(result == null) {
            return false
        } else {
            
            // compareSync(dataToBeCompared, encryptedData)
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect) {
                return { access: auth.createAccessToken(result)}
            } else {
                return false
            }
        }
    })
}

// Retrieve user's profile --- activity
/* module.exports.getProfile = async (userId) => {

    return User.findById(userId).then((result, error) => {

		if(error) {
			return false
		} else {
            result.password = ""
			return result 
		}
	})

} */

module.exports.getProfile = (data) => {

    return User.findById(data.userId).then((result, error) => {

		if(error) {
			return false
		} else {
            result.password = ""
			return result 
		}
	})

}

// Only regular users are allowed to enroll --- Activity 36
module.exports.enroll = async (userId, courseId) => {

    let isUserUpdated = await User.findById(userId).then(user => {

        user.enrollments.push({courseId: courseId});

        return user.save().then((user,error) => {

            if(error){
                return false
            } else {
                return true
            }
        })
    })

    let  isCourseUpdated = await Course.findById(courseId).then(course => {

        course.enrollees.push({userId: userId});

        return course.save().then((course,error) => {

            if (error){
                return false
            } else {
                return true
            }
        })
    })

    if(isUserUpdated && isCourseUpdated) {
        return "User is enrolled."
    } else {
        return false
    }

}