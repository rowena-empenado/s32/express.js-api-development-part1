const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

// Route to create a course
/* router.post("/", (req, res) => {

    courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
}) */



/* 
    Activity - Session 34

    1. Refactor the course route to implement user authentication for the admin when creating a course.
    2. Refactor the addCourse controller method to implement admin authentication for creating a course.
    3. Push to git with the commit message of Add activity code - S34.
    4. Add the link in Boodle.
*/

router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    // console.log(userData);

    // console.log(userData.isAdmin)

    courseController.addCourse(userData.isAdmin,req.body ).then(
        resultFromController => {
            if(resultFromController == false) {
                res.send("User is not allowed to create a course.");
            } else {
                res.send(resultFromController);
            }
        }
    )
        
})


// Route for retrieving all courses
router.get("/all", auth.verify, (req,res) => {

    courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})


/* Mini-Activity

    Create a route and controller for retrieving all active courses. No need to log in.

*/
router.get("/", (req, res) => {

    courseController.getAllActive(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving a specific course
router.get("/:courseId", (req, res) => {

    console.log(req.params.courseId)

    courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {

    const data = {
        courseId: req.params.courseId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        updatedCourse: req.body
    }

    courseController.updateCourse(data).then(resultFromController => res.send(resultFromController));

})

// Archive a course --- Activity (Session 35)
router.put("/:courseId/archive", auth.verify, (req, res) => {

    const data = {
        courseId: req.params.courseId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController));
})


module.exports = router;
