const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");

// Route for checking if email exists
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));

})

// Route for User Registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user authentication
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

/* 
    Activity:

    1. Create a /details route that will accept the user’s Id to retrieve the details of a user.
    2. Create a getProfile controller method for retrieving the details of the user:
    a. Find the document in the database using the user's ID
    b. Reassign the password of the returned document to an empty string
    c. Return the result back to the frontend
    3. Process a POST request at the /details route using postman to retrieve the details of the user.
    4. Push to git with the commit message of Add activity code - S33.
    5. Add the link in Boodle.
*/

/* router.post("/details", (req, res) => {
    userController.getProfile(req.body.id).then(
        resultFromController => {
            if(resultFromController == false) {
                res.send("User ID does not exist.")
            } else {
                res.send(resultFromController)
            }
        }
    )
}) */


// The auth.verify will acts as a middleware to ensure that the user is logged in before they can get the details
router.get("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    // console.log(userData);

    userController.getProfile({userId: userData.id }).then(
        resultFromController => {
            if(resultFromController == false) {
                res.send("User ID does not exist.")
            } else {
                res.send(resultFromController)
            }
        }
    )
})


// Only regular users are allowed to enroll --- Activity 36
router.post("/enroll", auth.verify, (req,res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        
        res.send("You are an admin. You cannot enroll into this course.")
    } else {

        userController.enroll(userData.id, req.body.courseId).then(resultFromController => res.send(resultFromController))
    }

})



module.exports = router;